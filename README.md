# Ansible Tools

## Development

```
pip3 install virtualenv
virtualenv venv
. venv/bin/activate
```

```
pip install -r requirements.txt
```

```
pip install black flake8
```

```
black --check *.py
flake8 *.py
```
